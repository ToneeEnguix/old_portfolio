import React, { useState } from "react";
import arrowLeft from "../Pictures/keyboard_arrow_left-24px.svg";
import { Helmet } from "react-helmet";

const Projects = () => {
  const [i, setI] = useState(0);

  const projects = [
    {
      title: "Barcelona Code School Cloning",
      img:
        "https://res.cloudinary.com/flameconnect/image/upload/v1601983170/project1_iqgfuy.png",
      description:
        "It is a clone of their website: barcelonacodeschool.com using only HTML and CSS. Most of the links are working and they will redirect you to their actual site. Their font-family is pay-to-use so I had to use another one, which makes the resembling less accurate. I found this very useful for the basic understanding of HTML + CSS.",
      link: "http://barcelonacodeschool.surge.sh",
      code: "https://gitlab.com/ToneeEnguix/bcs-cloning-project",
    },
    {
      title: "New York Times Cloning",
      img:
        "https://res.cloudinary.com/flameconnect/image/upload/v1601983170/project2_ulkes7.png",
      description:
        "It is a clone of the New York Times website. (www.nytimes.com) using only HTML and CSS. There is no link functionality but a mere cloning practice. I found this cloning to be extremely useful for the further understanding of 'flex' and 'grid' properties in CSS.",
      link: "http://nytimescloning.surge.sh/",
      code: "https://gitlab.com/ToneeEnguix/nytimes-cloning",
    },
    {
      title: "Website for a Music Label",
      img:
        "https://res.cloudinary.com/flameconnect/image/upload/v1601983176/project3_kntcim.png",
      description:
        "It is a website for a local music-label using only HTML and CSS. It is fully working with actual content and links to their other online sites like Discogs. I found it very useful for learning to build websites from sctrach.",
      link: "http://ultralocal.surge.sh/",
      code: "https://gitlab.com/ToneeEnguix/music-label-website",
    },
    {
      title: "ToDoer App",
      img:
        "https://res.cloudinary.com/flameconnect/image/upload/v1601983169/project4_d9vf8k.png",
      description:
        "This is a ToDo App. The best project to start testing your actual knowledge. I built this as practice for the Barcelona Code School Full-Stack Bootcamp. It is made using React and little CSS.",
      link: "http://todoerapp.surge.sh/",
      code: "https://gitlab.com/ToneeEnguix/todos",
    },
    {
      title: "Marley Media",
      img:
        "https://res.cloudinary.com/flameconnect/image/upload/v1601983170/project5_viixxj.png",
      description:
        "This is a website for Marley Media. It includes a landing page, project pages, a form to apply for a website and an admin dashboard for modifications. Built using MERN.",
      link: "http://paraquepuedasver.surge.sh/",
      code: "https://gitlab.com/ToneeEnguix/marley-media",
    },
    {
      title: "JS Sandbox",
      img:
        "https://res.cloudinary.com/flameconnect/image/upload/v1601983169/project6_xdrsu4.png",
      description:
        "This is a Sandbox to practice. It has 5 tabs: Rock, Paper, Scissors Game, Match Me (test by prompts Y/N ), Guess The Number Game, ROT13 Encoder and Currency Converter (Live)",
      link: "http://itsjsgames.surge.sh/",
      code: "https://gitlab.com/ToneeEnguix/js-sandbox",
    },
  ];

  const handleClick = (direction) => {
    if (direction === "l") {
      if (i > 0) {
        setI(i - 1);
      } else {
        setI(projects.length - 1);
      }
    } else if (direction === "r") {
      if (i < projects.length - 1) {
        setI(i + 1);
      } else {
        setI(0);
      }
    }
  };

  return (
    <div className="projectsDiv section">
      <Helmet>
        <title>Toni's Portfolio | Projects</title>
      </Helmet>
      <div className="p_container">
        <div onClick={() => handleClick("l")}>
          <img
            src={arrowLeft}
            alt="arrowLeft"
            className="p_arrow p_arrowLeft"
          />
        </div>

        <div className="p_content">
          <h2 className="p_contentH bold">{projects[i].title}</h2>
          <div className="p_imgandp">
            <img src={projects[i].img} alt="snapshot" className="snapshot" />
            <div className="p_descriptionContainer">
              <p className="p_description">{projects[i].description}</p>
            </div>
          </div>

          <div className="p_btnCont">
            <button
              className="p_button codeButton"
              onClick={() => {
                window.open(projects[i].code, "_blank");
              }}
            >
              Code
            </button>
            <button
              className="p_button siteButton"
              onClick={() => {
                window.open(projects[i].link, "_blank");
              }}
            >
              Site
            </button>
          </div>
        </div>

        <div onClick={() => handleClick("r")}>
          <img
            src={arrowLeft}
            alt="arrowLeft"
            className="p_arrow p_arrowRight"
          />
        </div>
      </div>
    </div>
  );
};

export default Projects;
