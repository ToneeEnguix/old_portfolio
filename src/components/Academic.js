import React from "react";
import open from "../Pictures/open.svg";
import { Helmet } from "react-helmet";

const Academic = () => {
  return (
    <div className="academicDiv section">
      <Helmet>
        <title>Toni's Portfolio | Academic</title>
      </Helmet>
      <h2>Courses:</h2>
      <ul>
        <li>JS Full-Stack Bootcamp @Barcelona Code School</li>
        <li>Responsive Web Design Certification @FreeCodeCamp</li>
        <li>Introduction To Domain Names and Web Hosting @Udemy</li>
        <li>Learn Swift @Codecademy</li>
        <li>Write JavaScritp for the Web @Open ClassRooms</li>
        <li>Fundamentals of Javascript @Google's Grasshopper</li>
        <li>Fundamentals of Digital Marketing @Google Garage</li>
        <a
          href="https://docs.google.com/spreadsheets/d/1B0cfbtIAl9V79XzQNrKjRSYUugLfghdn7RwboAGWOCI/edit?usp=sharing"
          target="_blank"
          rel="noopener noreferrer"
        >
          <div className="a_open">
            Full list
            <img src={open} />
          </div>
        </a>
      </ul>
    </div>
  );
};

export default Academic;
