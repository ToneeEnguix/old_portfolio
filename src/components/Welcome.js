import React from "react";

const Welcome = () => {
  return (
    <div className="welcomeDiv section">
      <div className="left">
        <h1 className="sectionTitle">
          Hi, I'm Toni,
          <span role="img" aria-label="xxxxx">
            🙅‍♂️
          </span>
        </h1>
        <h1 className="sectionTitle">Full-stack Developer</h1>
      </div>

      <h2 className="w_content right">
        I am actively looking for a job{" "}
        <span role="img" aria-label="xxxxx">
          🔍
        </span>
      </h2>

      <div className="w_contentCont">
        <h2 className="w_content">In the programming business</h2>
        <div className="w_grid2">
          <div className="w_listItem">
            Front-end
            <pre>
              {" "}
              <span role="img" aria-label="xxxxx">
                🎨
              </span>
            </pre>
          </div>
          <div className="w_listItem">
            Back-end
            <pre>
              {" "}
              <span role="img" aria-label="xxxxx">
                💻
              </span>
            </pre>
          </div>
          <div className="w_listItem">
            Full-stack
            <pre>
              {" "}
              <span role="img" aria-label="xxxxx">
                🤹‍♀️
              </span>
            </pre>
          </div>
          <div className="w_listItem">
            MERN-stack
            <pre>
              {" "}
              <span role="img" aria-label="xxxxx">
                🎱
              </span>
            </pre>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Welcome;
