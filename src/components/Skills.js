import React from "react";
import reactLogo from "../Pictures/expressjs.png";
import { Helmet } from "react-helmet";

const Skills = () => {
  return (
    <div className="skillsDiv section">
      <Helmet>
        <title>Toni's Portfolio | Skills</title>
      </Helmet>
      <div className="s_skillsCont">
        <div className="s_contentContainer">
          <h2 className="s_content">MongoDB</h2>
          <img
            alt="skills logo"
            src="https://victorroblesweb.es/wp-content/uploads/2016/11/mongodb.png"
            className="logo"
          />
        </div>
        <div className="s_contentContainerRight">
          <img alt="skills logo" src={reactLogo} className="logo smallLogo" />
          <h2 className="">Express</h2>
        </div>
        <div className="s_contentContainer">
          <h2 className="s_content">React</h2>
          <img
            alt="skills logo"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png"
            className="logo"
          />
        </div>
        <div className="s_contentContainerRight">
          <img
            alt="skills logo"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1280px-Node.js_logo.svg.png"
            className="logo"
          />
          <h2 className="s_content">Node.js</h2>
        </div>
        <div className="s_contentContainer">
          <h2 className="">HTML5</h2>
          <img
            alt="skills logo"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/512px-HTML5_logo_and_wordmark.svg.png"
            className="logo"
          />
        </div>
        <div className="s_contentContainerRight">
          <img
            alt="skills logo"
            src="https://icon-library.com/images/css-icon-png/css-icon-png-0.jpg"
            className="logo"
          />
          <h2 className="s_content">CSS3</h2>
        </div>
        <div className="s_contentContainer">
          <h2 className="">JavaScript</h2>
          <img
            alt="skills logo"
            src="https://seeklogo.com/images/J/javascript-logo-E967E87D74-seeklogo.com.png
"
            className="logo"
          />
        </div>
        <div className="s_contentContainerRight">
          <img
            alt="skills logo"
            src="https://secureservercdn.net/198.71.233.138/z3x.23d.myftpupload.com/wp-content/uploads/2019/08/react-native.png?time=1596796993"
            className="logo"
          />
          <h2 className="">React-Native</h2>
        </div>
        <div className="s_contentContainer">
          <h2 className="">AJAX</h2>
          <img
            alt="skills logo"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/AJAX_logo_by_gengns.svg/1280px-AJAX_logo_by_gengns.svg.png"
            className="logo"
          />
        </div>
        <div className="s_contentContainerRight">
          <img
            alt="skills logo"
            src="https://seeklogo.com/images/P/postman-logo-F43375A2EB-seeklogo.com.png"
            className="logo"
          />
          <h2 className="">Postman</h2>
        </div>
        <div className="s_contentContainer">
          <h2 className="s_content">Swift</h2>
          <img
            alt="skills logo"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Swift_logo.svg/1200px-Swift_logo.svg.png"
            className="logo"
          />
        </div>
        <div className="s_contentContainerRight">
          <h2 className="s_content">Git</h2>
          <img
            alt="skills logo"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Git_icon.svg/1200px-Git_icon.svg.png"
            className="logo"
          />
        </div>
        <div className="s_contentContainer">
          <h2 className="s_content">Surge</h2>
          <img
            alt="skills logo"
            src="https://cdn.worldvectorlogo.com/logos/surge.svg"
            className="logo"
          />
        </div>
      </div>
    </div>
  );
};

export default Skills;
