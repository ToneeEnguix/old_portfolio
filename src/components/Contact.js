import React from "react";
import arrow from "../Pictures/call_missed-24px.svg";
import { Helmet } from "react-helmet";

const Contact = () => {
  return (
    <div className="contactDiv section">
      <Helmet>
        <title>Toni's Portfolio | Contact</title>
      </Helmet>
      <h1 className="c_title">Find me here</h1>

      <div className="c_arrowContainer">
        <img src={arrow} alt="arrow" className="arrow" />
      </div>

      <div className="c_contentContainer">
        <div className="c_content1">
          <div className="c_content11">
            <p className="c_p">
              <span role="img" aria-label="xxxxx">
                📞
              </span>{" "}
              <a href="tel:+34628617641">+34 628 617 641</a>
            </p>
            <p className="c_p">
              <span role="img" aria-label="xxxxx">
                📩
              </span>{" "}
              <a href="mailto:tonies395@gmail.com">tonies395@gmail.com</a>
            </p>
            <p className="c_p">
              <span role="img" aria-label="xxxxx">
                🏡
              </span>{" "}
              Office or remote, I'm on board
            </p>
          </div>

          <div className="c_content12">
            <a
              href="https://api.whatsapp.com/send?phone=34628617641&text=Interview. Tomorrow."
              target="_blank"
              rel="noopener noreferrer"
              className="c_imgCont"
            >
              <img
                src="https://i.pinimg.com/originals/18/ba/0e/18ba0e411b1586feb65e97d7deb45b5d.png"
                alt="message logo"
                className="img whats"
              />
            </a>
            <a
              href="https://www.linkedin.com/in/tonienguix/"
              target="_blank"
              rel="noopener noreferrer"
              className="c_imgCont"
            >
              <img
                src="https://image.flaticon.com/icons/png/512/174/174857.png"
                alt="message logo"
                className="img"
              />
            </a>
            <a
              href="https://gitlab.com/users/ToneeEnguix/projects"
              target="_blank"
              rel="noopener noreferrer"
              className="c_imgCont"
            >
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1200px-GitLab_Logo.svg.png"
                alt="message logo"
                className="img"
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
