import React, { useState } from "react";
import "./index.css";
import Welcome from "./components/Welcome";
import Skills from "./components/Skills";
import Projects from "./components/Projects";
import Contact from "./components/Contact";
import Academic from "./components/Academic";
import { Helmet } from "react-helmet";

const App = () => {
  // wood pic = https://inhabitat.com/wp-content/blogs.dir/1/files/2018/02/wood.jpg

  // Background picture
  const desktopImage =
    "https://www.goodcore.co.uk/blog/wp-content/uploads/2019/08/coding-vs-programming-2.jpg";
  const mobileImage =
    "https://www.goodcore.co.uk/blog/wp-content/uploads/2019/08/coding-vs-programming-2.jpg";
  const imageUrl = window.innerWidth >= 650 ? desktopImage : mobileImage;

  const [page, setPage] = useState("welcome");

  const renderSection = () => {
    console.log("page: ", page);
    return page === "welcome" ? (
      <Welcome />
    ) : page === "skills" ? (
      <Skills />
    ) : page === "projects" ? (
      <Projects />
    ) : page === "contact" ? (
      <Contact />
    ) : (
      page === "academic" && <Academic />
    );
  };

  return (
    <div className="App" style={{ backgroundImage: `url(${imageUrl})` }}>
      <Helmet>
        <title>Toni's Portfolio | Home</title>
      </Helmet>
      <div className="render">
        <div
          className={page === "welcome" ? "selected" : "App-content"}
          id="welcome"
          onClick={(e) => setPage(e.target.id)}
        >
          <h2 className="title" id="welcome">
            Welcome
          </h2>
        </div>
        <div
          className={page === "skills" ? "selected" : "App-content"}
          id="skills"
          onClick={(e) => setPage(e.target.id)}
        >
          <h2 className="title" id="skills">
            Skills
          </h2>
        </div>
        <div
          className={page === "projects" ? "selected" : "App-content"}
          id="projects"
          onClick={(e) => setPage(e.target.id)}
        >
          <h2 className="title" id="projects">
            Projects
          </h2>
        </div>
        <div
          className={page === "academic" ? "selected" : "App-content"}
          id="academic"
          onClick={(e) => setPage(e.target.id)}
        >
          <h2 className="title" id="academic">
            Academic
          </h2>
        </div>
        <div
          className={page === "contact" ? "selected" : "App-content"}
          id="contact"
          onClick={(e) => setPage(e.target.id)}
        >
          <h2 className="title" id="contact">
            Contact
          </h2>
        </div>
      </div>
      {renderSection()}
    </div>
  );
};

export default App;
